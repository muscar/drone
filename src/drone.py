# -*- coding: utf-8 -*-

import argparse

from collections import namedtuple

import os

import sys

from urllib.parse import urlparse

import docker

import pygit2 as git

import yaml


Thunk = namedtuple("Thunk", ["fun", "args", "kwargs"])


def lazy(f, *args, **kwargs):
    return Thunk(f, args, kwargs)


def force(thunk):
    return thunk.fun(*thunk.args, **thunk.kwargs)


class CommandError(Exception):
    def __init__(self, message, failing_step):
        super().__init__(message)
        self.failing_step = failing_step


def git_clone_repo(repo_url, repo_dir_path):
    """
    Fetch app source code.
    """
    print(f"Clone {repo_url} into {repo_dir_path}")
    git.clone_repository(repo_url, repo_dir_path)
    return True


def git_checkout_branch(repo_dir_path, commit, branch_name):
    """
    Prepare branch from target commit.
    """
    print (
        f"Check out branch {branch_name} based on commit "
        f"{branch_name}"
    )

    repo = git.Repository(repo_dir_path)
    commit = repo.revparse_single(commit)
    try:
        branch = repo.branches[branch_name]
    except KeyError:
        print(
            f"# NOTE: The branch {branch_name} did not exist, so "
            "it was created"
        )
        branch = repo.create_branch(branch_name, commit)
    repo.checkout(branch)
    return True


def git_fetch_remote(repo_dir_path, remote_name):
    """
    Fetch remote.
    """
    print(f"Fetch remote '{remote_name}'")
    repo = git.Repository(repo_dir_path)
    remote = repo.remotes[remote_name]
    remote.fetch()
    return True


def docker_build_image(repo_dir_path, tag, force=False, clean_build_images=True):
    """
    Build docker image.
    """
    print(
        f"Build docker image from the repo {repo_dir_path}, "
        f"and tag it {tag}"
    )
    client = docker.from_env()
    image_exists = any(tag in img.tags for img in client.images.list())
    if not image_exists or force:
        # XXX Because of a bug we can't set pull to True (see https://github.com/moby/moby/issues/33946)
        client.images.build(
            path=repo_dir_path, tag=tag, rm=True, forcerm=True, pull=False
        )
    else:
        print(f"An image with the tag {tag} already exists. Skipping..")
    if clean_build_images:
        dangling_images = client.images.list(filters={"dangling": True})
        for img in dangling_images:
            client.images.remove(img.id)
    return True


def docker_pull_image(name, force=False):
    """
    Pull docker image.
    """
    print(f"Pull image {name}")
    client = docker.from_env()
    image_exists = any(name in img.tags for img in client.images.list())
    if not image_exists or force:
        client.images.pull(name)
    else:
        print(f"An image named {name} already exists. Skipping..")
    return True


def docker_run_container(image_name, container_name, config=None, detached=False):
    """
    Run docker container.
    """
    if detached:
        print(
            f"Run a container named {container_name} from the image "
            f"{image_name} in the background"
        )
    else:
        print(
            f"Run a container named {container_name} from the image {image_name}"
        )

    client = docker.from_env()

    container_is_running = any(
        container_name == cont.name
        for cont in client.containers.list()
    )

    if container_is_running:
        print(
            f"A container with the name {container_name} is "
            "already running. Skipping.."
        )
        return True

    container_exists = any(
        container_name == cont.name
        for cont in client.containers.list(filters={"status": "exited"})
    )

    if container_exists:
        container = client.containers.get(container_name)
        print(f"Container with name {container_name} already exists, but is stoppped. Starting..")
        container.start()
        return True

    port_mapping = {}
    ports = config.get("ports", [])
    links = config.get("links", [])
    environment = config.get("environment", [])
    environment.append(f"VIRTUAL_HOST={container_name}.dev")
    if ports:
        for mapping in ports:
            if ":" in mapping:
                host_port, container_port = mapping.split(":")
                port_mapping[f"{container_port}/tcp"] = host_port
            else:
                port_mapping[f"{mapping}/tcp"] = None
    client.containers.run(
        image_name, name=container_name, environment=environment,
        detach=detached, ports=port_mapping, publish_all_ports=True,
        links=links
    )
    return True


def docker_stop_container(container_name, rm):
    """
    Stop docker container.
    """
    print(f"Stop container {container_name}")
    client = docker.from_env()
    cont = client.containers.get(container_name)
    cont.stop()
    if rm:
        print(f"Remove container {container_name}")
        cont.remove()
    return True


class Command:
    def __init__(self, app_template):
        self._steps = []
        self._app_template = app_template

    def add_step(self, step):
        self._steps.append(step)

    def add_many_steps(self, steps):
        self._steps.extend(steps)

    def run(self, verbose=False, dry=False):
        for idx, step in enumerate(self._steps, start=1):
            if verbose:
                print(f"# Step {idx}")
                print(f"# {step.fun.__doc__.strip()}")
            success = force(step)
            if not success:
                raise CommandError("Failed to run command", failing_step=step)


class StartAppCommand(Command):
    @staticmethod
    def register_args(arg_parser):
        sub_parser = arg_parser.add_parser("start", help="Start the app")
        sub_parser.add_argument(
            "-f", "--force",
            action="store_const",
            const=True,
            default=False,
            help="Force pull git repository & rebuild images"
        )
        sub_parser.add_argument(
            "commit",
            metavar="SHA",
            help="The SHA of the commit to build the image from"
        )

    def build(self, commit, force=False):
        fetch_repo_steps, repo_attrs = fetch_repo_at_commit(
            self._app_template, commit
        )
        build_image_steps, image_attrs = build_docker_image(
            self._app_template, repo_attrs["repo_dir_path"], commit,
            force=force
        )
        pull_resource_images_steps, res_image_attrs = pull_resource_images(
            self._app_template, force=force
        )
        run_resource_containers_steps, res_cont_attrs = run_resource_containers(
            self._app_template, commit, res_image_attrs
        )
        run_main_container_steps, _ = run_main_container(
            self._app_template, commit, image_attrs["docker_image_tag"],
            res_cont_attrs
        )
        self.add_many_steps(fetch_repo_steps)
        self.add_many_steps(build_image_steps)
        self.add_many_steps(pull_resource_images_steps)
        self.add_many_steps(run_resource_containers_steps)
        self.add_many_steps(run_main_container_steps)


class StopAppCommand(Command):
    @staticmethod
    def register_args(arg_parser):
        sub_parser = arg_parser.add_parser("stop", help="Stop the app")
        sub_parser.add_argument(
            "--rm",
            action="store_const",
            const=True,
            default=False,
            help="Remove the containers after stopping them"
        )
        sub_parser.add_argument(
            "commit",
            metavar="SHA",
            help="The SHA of the commit to build the image from"
        )

    def build(self, commit, rm=False):
        for res, res_data in self._app_template["resources"].items():
            container_name = f"{self._app_template['app']['name']}-{commit}-{res}"
            self.add_step(lazy(docker_stop_container, container_name, rm=rm))
        container_name = f"{self._app_template['app']['name']}-{commit}"
        self.add_step(lazy(docker_stop_container, container_name, rm=rm))


def fetch_repo_at_commit(app_template, commit):
    repo_url = app_template["app"]["repo"]
    url_parts = urlparse(repo_url)
    repo_url_path = url_parts.path.strip("/")
    repo_dir_path = os.path.join(os.path.curdir, repo_url_path)
    branch_name = f"build-{commit}"
    steps = []
    if os.path.exists(repo_dir_path):
        steps.append(lazy(git_fetch_remote, repo_dir_path, "origin"))
    else:
        steps.append(lazy(git_clone_repo, repo_url, repo_dir_path))
    steps.append(lazy(git_checkout_branch, repo_dir_path, commit, branch_name))
    derived_data = {
        "repo_dir_path": repo_dir_path,
    }
    return steps, derived_data


def build_docker_image(app_template, repo_dir_path, commit, force):
    repo_url = app_template["app"]["repo"]
    url_parts = urlparse(repo_url)
    repo_name, _ = os.path.splitext(url_parts.path)
    tag = "{}:{}".format(repo_name.strip("/"), commit)
    steps = [lazy(docker_build_image, repo_dir_path, tag, force=force)]
    derived_data = {
        "docker_image_tag": tag,
    }
    return steps, derived_data


def pull_resource_images(app_template, force):
    steps = []
    derived_data = {"images": {}}
    for res, res_data in app_template["resources"].items():
        steps.append(lazy(docker_pull_image, res_data["image"], force=force))
        derived_data["images"][res] = res_data["image"]
    return steps, derived_data


def run_resource_containers(app_template, commit, res_image_attrs):
    steps = []
    derived_data = {"containers": {}}
    for res, image in res_image_attrs["images"].items():
        container_name = f"{app_template['app']['name']}-{commit}-{res}"
        container_config = app_template["resources"][res]
        steps.append(
            lazy(
                docker_run_container, image, container_name, container_config, detached=True
            )
        )
        derived_data["containers"][res] = container_name
    return steps, derived_data


def run_main_container(app_template, commit, image, res_cont_attrs):
    container_name = f"{app_template['app']['name']}-{commit}"
    container_config = app_template["app"]
    container_config["links"] = {
        res_cont_attrs["containers"][res]: res
        for res in container_config["links"]
    }
    steps = [
        lazy(docker_run_container, image, container_name, container_config, detached=True)
    ]
    return steps, {}


COMMANDS = {
    "start": StartAppCommand,
    "stop": StopAppCommand,
}


def main():
    parser = argparse.ArgumentParser(prog="drone")
    parser.add_argument(
        "-t", "--template",
        dest="app_template",
        default="drone.yaml",
        type=argparse.FileType("r"),
        help="App config file"
    )
    subparsers = parser.add_subparsers(dest="command")
    StartAppCommand.register_args(subparsers)
    StopAppCommand.register_args(subparsers)
    args = parser.parse_args(sys.argv[1:])
    args_dict = vars(args)
    subcommand = args_dict.pop("command")
    app_template = yaml.load(args_dict.pop("app_template"))

    cmd_class = COMMANDS.get(subcommand, None)
    if cmd_class is None:
        raise ValueError(f"Unknown subcommand '{subcommand}'")
    command = cmd_class(app_template)
    command.build(**args_dict)

    try:
        command.run(verbose=True, dry=False)
    except CommandError as e:
        print(f"{str(e)}:\n\nFailing step:\n\n{e.failing_step}")


if __name__ == "__main__":
    main()
