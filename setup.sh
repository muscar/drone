#!/bin/bash

echo "Running dnsmasq.."
docker run -d --name dnsmasq --restart always -p 53535:53/tcp -p 53535:53/udp --cap-add NET_ADMIN andyshinn/dnsmasq --address=/dev/127.0.0.1

echo "Running nginx-proxy.."
docker run -d -p 80:80 -p 443:443 --name nginx-proxy --restart always -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy

echo "Ok, done. Now you have to add 127.0.0.1:53535 to your DNS servers list."
echo -e "If you're on macOS you can run:\n\nsudo mkdir /etc/resolver\nsudo tee -a /etc/resolver/dev\nnameserver 127.0.0.1\nport 53535\n^D\n"
