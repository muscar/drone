# Drone -- A tool to automatically provision and run multiple versions of an app

Drone helps developers test multiple versions of dockerized apps locally. It
tries to be familiar to `docker` and `docker-compose` users by using a very
similar config file format, and command line arguments.

## Install

Clone this repo, and (preferably in a virtual env) run:

```
$ pip install -r requirements.txt

$ ./setup.sh
```

This will pull the app dependencies, as well as help you set up your machine to
access apps using more meaningful hostnames. It uses `dnsmasq` and `nginx-proxy`
begind the scenes.


## App template files

Apps are configured by defining an "app template" file (`drone.yml` by default).
The file must be located in the root folder of the directory you want to use to
develop your app. The file format is very similar to `docker-compose` files.

A few things to note: the app is defined under the `app` key, and the name must
be explicitly specified. App resources live under the `resources` key, and are
keyed with their own name. Instead of specifying an image, the app config
section must give the path to the root of a git repo under the key `repo`.
Other options are similar to the ones in `docker-compose files`. Here's the
template file for the sample app:

```yaml
app:
  name: muninn-go
  repo: https://gitlab.com/muscar/muninn-go.git
  links:
    - db
  ports:
    - "8080"
  environment:
    - DB_ADDR=db:5432
    - DB_USER=postgres

resources:
  db:
    image: postgres:9.6
    ports:
      - "5432"
```

## Starting an app version

Once you've defined an app template file, it's enough to use the `start` command:

```
drone.py start e0a0880c
```

This will pull the source of the app from the repo (if not already present), it
will build, and tag the app image, pull the resource images, start and give
easy to find names to the resource containers, and finally start the main
container, link the main container to the resource containers, and give it an
easy to find name:

```
$ drone.py start e0a0880c
# Step 1
# Fetch app source code.
Clone https://gitlab.com/muscar/muninn-go.git into ./muscar/muninn-go.git
# Step 2
# Prepare branch from target commit.
Check out branch build-e0a0880c based on commit build-e0a0880c
# NOTE: The branch build-e0a0880c did not exist, so it was created
# Step 3
# Build docker image.
Build docker image from the repo ./muscar/muninn-go.git, and tag it muscar/muninn-go:e0a0880c
# Step 4
# Pull docker image.
Pull image postgres:9.6
# Step 5
# Run docker container.
Run a container named muninn-go-e0a0880c-db from the image postgres:9.6 in the background
# Step 6
# Run docker container.
Run a container named muninn-go-e0a0880c from the image muscar/muninn-go:e0a0880c in the background
```

In the above listing, you can see drone starting an app called `muninn-go` from
commit `e0a0880c`. We can now ping our app:

```
$ ping -c1 muninn-go-e0a0880c.dev
PING muninn-go-e0a0880c.dev (127.0.0.1): 56 data bytes
64 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=0.047 ms
```

## Stopping an app version

To bring down and app version container, and its linked resources, use the
`stop` command:

```
$ drone.py stop --rm e0a0880c
# Step 1
# Stop docker container.
Stop container muninn-go-e0a0880c-db
Remove container muninn-go-e0a0880c-db
# Step 2
# Stop docker container.
Stop container muninn-go-e0a0880c
Remove container muninn-go-e0a0880c
```
